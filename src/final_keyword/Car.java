package final_keyword;

public final class Car {

	String Mrf;
	String model;
	double price;
	int wheels;
	
	
	public final void start(){
		System.out.println("Car is starting on");
	}
	
	public void stop(){
		System.out.println("Car is stopping");
	}
}
