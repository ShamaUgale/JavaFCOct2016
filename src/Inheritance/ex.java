package Inheritance;

public class ex {

	public static void main(String[] args) {

		
		Car c= new Car();
		c.model="city";
		c.Mrf="Honda";
		c.price=88888888.00;
		c.wheels=6;
		
		
		
		c.start();
		c.stop();
		
		
		SportsCar s= new SportsCar();
		s.model="xyz";
		s.Mrf="BMW";
		s.price=99999999.00;
		s.wheels=9;
		s.speed=5555;
		
		s.start();
		s.accelerate();
		s.stop();
	}

}
