package basics;

public class ifs {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		System.out.println("*************** adult/minor *********************");
		
		
		int age=0;
		
		if(age>=18){// if this cond evaluates to true , den if block is executed
			System.out.println("ADULT");
		}else{ // if the condition becomes false , den else block is executed
			System.out.println("MINOR");
		}
		
		System.out.println("***************** greater num ***********************");
		int a=560;
		int b=560;
		int c=560;
		
		if(a>b  && a>c){
			System.out.println("A is greater");
		}else if(b>a  && b>c){
			System.out.println("B is greater");
		}else if(c>a && c>b){
			System.out.println("C is greater");
		}else{
			System.out.println("NONE");
		}
		
		
	}

}
