package basics;

public class operators {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// arith
		// +  - /  *  %
		
		
		int i=90;
		int j=67;
		
		System.out.println("i+j = " + (i+j));
		System.out.println("i-j = " + (i-j));
		System.out.println("i/j = " + (i/j));
		System.out.println("i*j = " + (i*j));
		System.out.println("i%j = " + (i%j));
		
		
		// comparison/conditional ops
		
		// >   <   >=    <=    == !=  
		// these will  return boolean 
		
		System.out.println("i>j = " + (i>j));
		
		System.out.println("i<j = " + (i<j));
		
		System.out.println("i==j = " + (i==j));
		System.out.println("i!=j = " + (i!=j));
		
		
		// AND  &&
		//OR ||
		//NOT    !
		
		// a b c
		
		int k=98;
		
		System.out.println("i>j  and i>k ??? "+ (i>j  &&  i>k));
		
		System.out.println("i>j  or i>k ??? "+ (i>j  ||  i>k));
		
		
		// AND -- true only if all conditions are true , else false
		//OR -- false only if all conditions are false , else true
		
		
		System.out.println("  NOT i>j   ??? "+ (! (i>j)));
		
		
		
		
		// increment or decrement ops
		
		// ++  --
		// x++ ====>  x , x+1
		//++x  =====>  x+1, x
		
		int x=90;
		System.out.println(x++); // use/print x, den do x+1
		System.out.println(x);
		
		System.out.println(++x); // do x+1 , den use it/print it
		System.out.println(x);
		
		System.out.println(x--);
		System.out.println(x);
		
		System.out.println(--x);
		System.out.println(x);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
