package Arrays;

public class twoDim {

	public static void main(String[] args) {

		int[][] arr= new int[3][2];// [rows][cols]
		
		//0th row
		arr[0][0]=78;
		arr[0][1]=90;
		
		//1st row
		arr[1][0]=56;
		arr[1][1]=934;
		
		//2nd row
		arr[2][0]=67;
		arr[2][1]=23;
		
		
		
		System.out.println(arr[0][1]);
		System.out.println("Rows : " + arr.length);
		System.out.println("Cols : "+ arr[0].length);
		
		int rows= arr.length;
		int cols = arr[0].length;
		
		for(int i=0;i<rows; i++){// i=0,i=1 , 2, 3
			for(int j=0;j<cols;j++){//i=0,j=0  .. j=1
				System.out.println(arr[i][j]);
			}
		}
		
	}

}
