package constrcutors;

public class exx {

	public static void main(String[] args) {
 // overloading == compile time polymorphism
		
		
		
		// overriding is runtime polymorphism
		Car c= new Car("Hyundai", "Verna", 66666.88, 4);
//		c.model="City";
//		c.Mrf="Honda";
//		c.price=777.9;
//		c.wheels=8;
		
		
		System.out.println(c.model);
		
		c.start();
		
		
		SportsCar s= new SportsCar("Mahindra", "Xylo", 88888.99, 5, 455);
		System.out.println(s.model);
		
	}

}
