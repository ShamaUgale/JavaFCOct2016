package abstraction;

public abstract class Loan {
	
	
	String accNo;
	double loanAmount;
	
	public void apply(){
		System.out.println("Applying for loan");
	}
	
	public abstract void submitDocs();

	public void sactionLoan(){
		System.out.println("Loan is sanctioned");
	}
	
	public abstract void calculateInterest();
	
	public abstract void emi();
	
	public void repay(){
		System.out.println("Repaying the loan");
	}
	
	public abstract void close();
}
