package collectionApis;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class hashmap {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		
		HashMap map= new HashMap();
		map.put("shama", "576686767878");
		map.put("aarush", "88888888888");
		map.put("deepak", "9999999999999");
		map.put("santosh", "66666666666666");
		
		
		System.out.println("Size : "+ map.size());
		System.out.println(map);
		
		System.out.println(map.get("shama"));
		
		System.out.println(map.remove("shama"));
		
		System.out.println(map);
		
		
		map.put("deepak", "222222222222");
		
		System.out.println(map);
		
		map.put("Deepak", "9999999999999");
		System.out.println(map);
		
		
		Set s= map.keySet();
		
		Iterator it= s.iterator();
		while(it.hasNext()){
			System.out.println(map.get(it.next()));
		}
		
	}

}
