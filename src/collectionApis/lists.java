package collectionApis;

import java.util.ArrayList;
import java.util.Iterator;

public class lists {

	/**
	 * 1. Lists are index based
	 * 2. Lists maintain insertion order 
	 * 3. They hold duplicates
	 */
	public static void main(String[] args) {

		ArrayList li= new ArrayList();
		
		System.out.println("The arraylist size : "+ li.size());
		
		li.add("Shama");
		
		System.out.println("The arraylist size : "+ li.size());

		li.add("Shama");
		li.add("Spruha");
		li.add("Aarush");
		li.add("Deepak");
		li.add("Aarush");
		li.add("Deepak");
		li.add("Aarush");
		li.add("Deepak");
		li.add("Aarush");
		li.add("Deepak");
		li.add("Aarush");
		li.add("Deepak");
		System.out.println("The arraylist size : "+ li.size());

		System.out.println(li);
		
		li.add(2, "Santosh");
		System.out.println(li);
		
		
		ArrayList l1= new ArrayList();
		l1.add("12345");
		l1.add("456765");
		
		l1.add("23567");
		l1.add("989876");
		
		
		
		li.addAll(l1);
		System.out.println(li);
		
		li.addAll(3,l1);
		System.out.println(li);
		
		
		System.out.println("The arraylist size : "+ li.size());

		li.remove(3);
		
		
System.out.println(li);
		
		
		System.out.println("The arraylist size : "+ li.size());

		li.remove("Shama");
		
System.out.println(li);
		
		
		System.out.println("The arraylist size : "+ li.size());

		
		
		System.out.println("Get the 0th element : " + li.get(0));
		
		System.out.println("is Shama in list ?  "+ li.contains("Shama"));
	
		
		System.out.println("******************** fetch all elements from the list ******************************");
		
		for(int i=0; i<li.size();i++){
			System.out.println(li.get(i));
		}
		
		System.out.println("*******************");
		
Iterator it= li.iterator();
		
		while(it.hasNext()){
			System.out.println(it.next());
		}
	}

}
