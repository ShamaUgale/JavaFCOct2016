package exceptionHandling;

public class throwKeyword {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {

		String s= "P1";
		
		if(s.equalsIgnoreCase("Blocker")){
			throw new Exception("Blocker");
		}else if(s.equalsIgnoreCase("P1")){
			throw new Exception("P1 issue");
		}
		
		
	}

}
