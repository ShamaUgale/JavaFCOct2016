package exceptionHandling;

public class example {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int i;
		int j;
		int z = 0;

		try{
			i=9;
			j=6;
			z=i/j;
			String str=null;
			str.split("[ ]");
		}catch(ArithmeticException e){
			System.out.println("ArithmeticException: "+e.getMessage());
		}catch (Exception e) {
			System.out.println(" Exception : "+e.getMessage());
		}catch(Throwable t){
			System.out.println(t.getMessage());
		}finally{
			System.out.println("In finally");
		}
		
		System.out.println("Z is : " + z);
		
		
	}

}
