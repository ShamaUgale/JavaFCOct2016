package polymorphism;

public class Calc {

	
	// overloading
	
	
	
	
	
	// in the same class - same function name - different set/type of parameters
	
	public void add(int a, int b){
		System.out.println("a+b : "+ (a+b));
	}
	
	public void add(int a, int b, int c){
		System.out.println("a+b+c : "+ (a+b+c));
	}
	
	public void add(String a, int b){
		System.out.println("a+b : "+ (a+b));
	}
	
	public void add(int a, int b,int c, int d){
		System.out.println("a+b : "+ (a+b+c+d));
	}
	
}
