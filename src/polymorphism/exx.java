package polymorphism;

public class exx {

	public static void main(String[] args) {
 // overloading == compile time polymorphism
		
		
		Calc cal= new Calc();
		cal.add(88, 77);
		
		cal.add("hello", 78);
		
		cal.add(22, 444, 9);
		
		// overriding is runtime polymorphism
		Car c= new Car();
		c.start();
		
		SportsCar s= new SportsCar();
		s.start();
	}

}
